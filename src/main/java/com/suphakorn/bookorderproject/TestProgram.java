/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.suphakorn.bookorderproject;

/**
 *
 * @author exhau
 */
public class TestProgram {
    public static void main(String[] args) {
        BookOrder order1 = new BookOrder(10);
        double buyorder1 = order1.BookBuy(10);
        System.out.println("Number of book = " + order1.getBookNum() + " Total price is " + buyorder1);
        BookOrder order2 = new BookOrder(12);
        double buyorder2 = order2.BookBuy(12,"discount");
        System.out.println("Number of book = " + order2.getBookNum() + " Total price is " + buyorder2);
    }
}
