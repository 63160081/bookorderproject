/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.suphakorn.bookorderproject;

/**
 *
 * @author exhau
 */
public class BookOrder {

    //number of book to oder
    private int BookNum;
    //discount code
    private String DiscountCode = "disocunt";
    //discount price 10%
    private double Discount = 10.00;

    public BookOrder(int BookNum) {
        this.BookNum = BookNum;
        this.DiscountCode = "disocunt";
        this.Discount = 10.00;
    }

    //Book price calculate method.
    public double BookBuy(int BookNum) {
        return BookNum * 120.00;
    }

    //Book price calculate with discout code method.
    public double BookBuy(int BookNum, String DiscountCode) {
        return (BookNum * 120.00) - ((10/100) * (BookNum * 120.00));
    }

    public int getBookNum() {
        return BookNum;
    }

}
